import './App.css';

import WhetherCard from './Components/WhetherCard';

import React, { Component } from 'react'

export class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      weatherList: [],
      dayArray: ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'],
      errorMsg: null,
    }
  }


  componentDidMount() {
    fetch('https://api.openweathermap.org/data/2.5/forecast?lat=12.9767936&lon=77.590082&cnt=40&appid=85bc6ecbc4f23428bf79a8f63040664c')
      .then((response) => {
        if (!response.ok) {
          throw Error('Could not fetch the data for this url, check the url and try again')
        }
        return response.json()
      })
      .then(data => {
        this.setState({
          weatherList: data.list,
          errorMsg: ''
        })
      })
      .catch(err => {
        this.setState({
          errorMsg: err.message,
        })
      })
  }

  render() {
    const { weatherList, dayArray, errorMsg } = this.state
    if (!errorMsg) {
      return (
        <div className="App d-flex flex-column justify-content-center align-items-center align-content-center">
          <p className='pt-5 p-2 h2'>Weather Forecast for 5 Days</p>
          <section className="d-flex flex-wrap justify-content-center align-items-center align-content-center">
            {weatherList.map((details, index) => {
              return <WhetherCard key={index} weather={details} dayArray={dayArray} />
            })}
          </section>
        </div>
      );
    } else {
      return (
        <div className='d-flex flex-row justify-content-center align-items-center align-content-center err-msg'>
          <h2 className='text-center m-5 p-5'>Currently We are unable to fetch the data. We are going to fix the issues try after some time.</h2>
        </div>
      )
    }

  }
}

export default App

